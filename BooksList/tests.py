from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import time

# Create your tests here.
class BooksListUnitTest(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_books_list_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'ListOfBook.html')

    def test_logout_url_is_exist(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 200)

    def test_logout_using_logout_function(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logout)

    def test_logout_using_logout_template(self):
        response = Client().get('/logout')
        self.assertTemplateUsed(response, 'logout.html')

    def test_favBooks_url_is_exist(self):
        response = Client().get('/favBooks/quilting')
        self.assertEqual(response.status_code, 200)

    def test_favBooks_using_favBooks_function(self):
        found = resolve('/favBooks/quilting')
        self.assertEqual(found.func, favBooks)

    # def test_favBooks_using_favBooks_template(self):
    #     response = Client().get('/favBooks/quilting')
    #     self.assertTemplateUsed(response, 'favouriteBooks.html')

    # def test_countUp_url_is_exist(self):
    #     response = Client().get('/countUp')
    #     self.assertEqual(response.status_code, 200)

    def test_countUp_using_countUp_function(self):
        found = resolve('/countUp')
        self.assertEqual(found.func, countUp)

    # def test_countDown_url_is_exist(self):
    #     response = Client().get('/countDown')
    #     self.assertEqual(response.status_code, 200)

    def test_countDown_using_countDown_function(self):
        found = resolve('/countDown')
        self.assertEqual(found.func, countDown)

    # def test_getCounter_url_is_exist(self):
    #     response = Client().get('/getCounter')
    #     self.assertEqual(response.status_code, 200)

    def test_getCounter_using_getCounter_function(self):
        found = resolve('/getCounter')
        self.assertEqual(found.func, getCounter)


    

