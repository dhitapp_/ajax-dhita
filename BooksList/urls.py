from django.urls import path
from django.conf.urls import url, include
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#url for app

urlpatterns = [
    path('', index, name = "index"),
    path('favBooks/<str:title>', favBooks, name = "favBooks"),
    path('logout', logout, name = "logout"),
    path('countUp', countUp, name="countUp"),
    path('countDown', countDown, name="countDown"),
    path('getCounter', getCounter, name="getCounter"),
    url(r'^auth/', include('social_django.urls', namespace='social'))
]