$(document).ready(function () {
    $( "#chall" ).click(function() {
        var bla = $('#challenge').val();
        $.ajax({
            url: '/favBooks/'+ bla,
            type: 'GET',
            success: function (data) {
                var teks = "";
                for (i = 0; i < data.items.length; i++) {
                    teks += "<tr><td>" + data.items[i].volumeInfo.title + "</td>" + "<td>";
                    for (j = 0; j < data.items[i].volumeInfo.authors.length; j++) {
                        teks +=  data.items[i].volumeInfo.authors[j];
                        if (j > 0) teks +=  ", " + data.items[i].volumeInfo.authors[j];
                    } teks += "</td><td>" + data.items[i].volumeInfo.publishedDate + "</td><td><img src=\"" +
                     data.items[i].volumeInfo.imageLinks.smallThumbnail +"\"/></td><td id=\"clicked\"><button type=\"button\" class=\"btn btn-link\" onclick=\"afterClicked(" + data.items.length + "," + i + ")\"><img src=\"https://i.gifer.com/ZfUY.gif\" width=\"150\" id=\"sign_" + i + "\")></button></td></tr>";
                } $('#books').append(teks);
            }
        });
      }); getCounter();
});

function afterClicked(data, sign) {
    var counter = $('#TextBox').val();
    var cek = "sign_" + sign;
    for ( j = 0; j < data; j++) {
        var cek_2 = "sign_" + j;
        if (cek.match(cek_2)) {
            var gambar = document.getElementById(cek_2);
            if (gambar.src.match("https://i.gifer.com/ZfUY.gif")) {
                gambar.src = "https://media.giphy.com/media/cI42MyKJvGtHTvSeuV/giphy.gif";
                inCount();
            } else if (gambar.src.match("https://media.giphy.com/media/cI42MyKJvGtHTvSeuV/giphy.gif")){ 
                deCount();
                gambar.src = "https://i.gifer.com/ZfUY.gif";
            }
        }
    }
};

function getCounter() {
    $.ajax({
        url:"/getCounter",
        dataType : 'json',
        success: function(result){
            $('#TextBox').attr('value', result); 
        }
    });
}

function inCount(){
    $.ajax({
        url:"/countUp",
        dataType : 'json',
        success: function(result){
            $('#TextBox').attr('value', result); 
        }
    });
};


function deCount() {
    $.ajax({
        url:"/countDown",
        dataType : 'json',
        success: function(result){
            $('#TextBox').attr('value', result); 
        }
    });
};