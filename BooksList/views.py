from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponse
import json
import requests
from django.contrib.auth import logout as auth_logout

# Create your views here.
response = {}
def index(request):
    if request.user.is_authenticated:
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
    return render(request, "ListOfBook.html", response)


def countUp(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')

def countDown(request):
    if request.user.is_authenticated:
        request.session['counter'] = request.session['counter'] - 1
        if request.session['counter'] < 0:
            request.session['counter'] = 0
        return HttpResponse(request.session['counter'], content_type = 'application/json')

def getCounter(request):
    if request.user.is_authenticated:
        return HttpResponse(request.session['counter'], content_type = 'application/json')

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return render(request, 'logout.html', {})

def favBooks(request, title):
    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + title)
    response = response.json()
    return JsonResponse(response)

